// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/http"
)

var addr = flag.String("addr", ":8080", "http service address")

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)

	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	switch r.URL.Path {
	case "/wasm_exec.js":
		w.Header().Set("Content-Type", "text/javascript;charset=utf-8")
		http.ServeFile(w, r, "wasm_exec.js")
	case "/test.wasm":
		w.Header().Set("Content-Type", "application/wasm;charset=utf-8")
		http.ServeFile(w, r, "test.wasm")
	case "/style.css":
		w.Header().Set("Content-Type", "text/css;charset=utf-8")
		http.ServeFile(w, r, "style.css")
	case "/":
		http.ServeFile(w, r, "home.html")
	default:
		http.Error(w, "Not found", http.StatusNotFound)
	}
}

func main() {
	flag.Parse()
	hub := newHub()
	go hub.run()
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
