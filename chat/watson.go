package chat

import (
	"fmt"
	"log"
	"strings"
	"syscall/js"
)

type watsontts struct {
	ws js.Value
//	audio []byte
	text string
	accept string
	chunks js.Value
}

const (
	SPEECH_TOKEN = "SPEECH_TOKEN"
	SPEECH_VOICE = "en-US_AllisonVoice"
	SPEECH_URI = "wss://stream.watsonplatform.net/text-to-speech/api/v1/synthesize"
	WATSON_CMD = "!watson"
	WATSON_CODE_OK = 1000
)

// SPEECH_TOKEN prefix, WATSON_CMD prefix
func speech(token string, cmd string) {
	log.Printf("Starting Watson websocket: %s\n %s\n", cmd, token)

	t := strings.TrimLeft(token, SPEECH_TOKEN)
	uri := fmt.Sprintf("%s?voice=%s&watson-token=%s",
		SPEECH_URI,
		SPEECH_VOICE,
		t)

	txt := strings.TrimLeft(cmd, WATSON_CMD)
	w := watsontts{text: txt, accept: "audio/wav"}
	a := js.Global().Get("Array").New(1)
	w.chunks = a
	w.ws = connect(uri, w.openWatsonCb, w.msgWatsonCb, w.closeWatsonCb, w.errorWatsonCb)
}

func (w *watsontts) openWatsonCb(event js.Value) {
	req := fmt.Sprintf("{\"text\": \"%s\", \"accept\": \"%s\"}", w.text, w.accept)

	log.Printf("Watson socket opened, sending: %v\n", req)

	w.ws.Call("send", req)
}

func (w *watsontts) msgWatsonCb(event js.Value) {
        dat := event.Get("data")
	if dat.Type() == js.TypeString {
		log.Println(dat.String())
	} else {

		//s := dat.String()
		//buf := []byte(s)
		//w.audio = append(w.audio, buf...)

		// accumulate audio chunks
		w.chunks.Call("push", dat)

		//DEBUG
		txt := fmt.Sprintf("audio data: %v", dat.String())
		log.Println(txt)
	}
}

// UI feedback to fire at end of connection
func (w *watsontts) closeWatsonCb(event js.Value) {
	code := event.Get("code").Int()
	rsn := event.Get("reason").String()
	cln := event.Get("wasClean").String()
	log.Printf("Watson code (%s) %s : %s \n", code, cln, rsn)
	if code == WATSON_CODE_OK {
		wnd := js.Global()
		// Form blob from chunks
		blob := wnd.Get("Blob").New(w.chunks)
		log.Println("chunks blobbed")
		audioUrl := wnd.Get("URL").Call("createObjectURL", blob)
		log.Println("audio url created")
		audio := wnd.Get("Audio").New(audioUrl)
		log.Println("audio playing")
		audio.Call("play")
	}
////	w.chunks.Release()
}

// UI feedback to fire at errors
func (w *watsontts) errorWatsonCb(event js.Value) {
////	w.chunks.Release()
	log.Println(event.String())
}

