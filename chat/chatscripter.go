package chat

import (
	"strings"
	"syscall/js"
)

type ChatScripter struct {
	rmpanel string
	ws      js.Value
}

// NewChatScripter makes a wrapper instance
func NewChatScripter(id string) *ChatScripter {
	// Set the panel ID that will be reference across the package
	// todo refactor
	rmpanel = id

	return &ChatScripter{
		rmpanel: id,
	}
}

// RecordMessage pushes the text data into the WebSocket from the frontend web UI side
func (u *ChatScripter) RecordMessage(txt string) {
	u.ws.Call("send", txt)
}

// NewVanityAddress obtains a BTC address with the specified prefix
func (u *ChatScripter) CalcVanityAddress(prefix string) (string, int, error) {
	return calcVanityAddress(prefix)
}

func (u *ChatScripter) HelloWatson(cmd string) {
	// 1. get token from the hub (because the hub isn't subject to x-site restriction)
	// 2. expect hub response, and dispatch from interpretDataFromHub
	u.ws.Call("send", cmd)
}

// Connect starts WebSocket to URL and event hooks
func (u *ChatScripter) Connect(url string) {
	u.ws = connect(url, nil, u.interpretDataFromHub, closeConnectionCb, nil)
}

func (u *ChatScripter) interpretDataFromHub(event js.Value) {
	d := event.Get("data").String()
	a := strings.Split(d, "\n")
	if strings.HasPrefix(a[0], SPEECH_TOKEN) {
		speech(a[0], a[1])
	} else {
		addRmPanelItemCb(d)
	}
}

// UI update to fire at message appearance
func addRmPanelItemCb(txt string) {
	s := strings.Split(txt, "\n")
	for _, v := range s {
		printToRmPanel(v)
	}
}

// UI feedback to fire at end of connection
func closeConnectionCb(event js.Value) {
	printToRmPanel("<b>Connection closed.</b>")
}

