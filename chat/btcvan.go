/* See Mastering Bitcoin, Example 4-8
 */
package chat

import (
	"errors"
	"strings"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
)

const ATTEMPT_MAX = 50000

// By default (without -testnet), use mainnet.
var chainParams = &chaincfg.MainNetParams

// calcVanityAddress obtains a BTC address with the specified prefix
func calcVanityAddress(prefix string) (string, int, error) {
        var count int
        for count = 1; count < ATTEMPT_MAX; count++ {
                addr, err := randomAddress()
                if err != nil {
                        return "", 0, err
                }

                if matchFound(prefix, addr) {
                        return addr, count, nil
                }
        }
        return "", count, errors.New("No match after max attempts")
}

// Case-insensitive otherwise search performance suffers
func matchFound(prefix string, address string) bool {
	lower := strings.ToLower(address)
	p := strings.ToLower(prefix)
	return strings.HasPrefix(lower, p)
}

// RandomAddress generate a random new public address
func randomAddress() (string, error) {
	// We don't expose prv key
	// because this was for fun/learning.
	privKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return "", err
	}

	addrPubKey, err := btcutil.NewAddressPubKey(
		privKey.PubKey().SerializeUncompressed(), chainParams)
	if err != nil {
		return "", err
	}

	rcvAddr := addrPubKey.AddressPubKeyHash().EncodeAddress()
	return rcvAddr, nil
}
