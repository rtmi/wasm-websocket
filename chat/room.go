package chat

import (

	"syscall/js"
)

type ehandler func(event js.Value)

var rmpanel string

// Connect starts WebSocket to URL and event hooks
func connect(url string, openCb ehandler, msgCb ehandler, closeCb ehandler, errorCb ehandler) js.Value {
	w := js.Global().Get("WebSocket")
	ws := w.New(url)
	if openCb != nil {
		opncb := js.NewEventCallback(js.PreventDefault, openCb)
		ws.Set("onopen", opncb)
	}
	if msgCb != nil {
		addcb := js.NewEventCallback(js.PreventDefault, msgCb)
		ws.Set("onmessage", addcb)
	}
	if closeCb != nil {
		clscb := js.NewEventCallback(js.PreventDefault, closeCb)
		ws.Set("onclose", clscb)
	}
	if errorCb != nil {
		errcb := js.NewEventCallback(js.PreventDefault, errorCb)
		ws.Set("onerror", errcb)
	}
	return ws
}

// print text to room panel UI
func printToRmPanel(txt string) {
        dom := js.Global().Get("document") 
        line := dom.Call("createElement", "div") 
        line.Set("innerHTML", txt)
        appendLog(line) 
} 
// add dom element to room panel UI
func appendLog(msg js.Value) {
	dom := js.Global().Get("document")
	pnl := dom.Call("getElementById", rmpanel)
	doScroll, newtop := calcScroll(pnl)
	pnl.Call("appendChild", msg)

	if doScroll {
		pnl.Set("scrollTop", newtop)
	}
}

func calcScroll(pnl js.Value) (bool, int) {
	// translate the scroll logic
	top := pnl.Get("scrollTop").Int()
	ht := pnl.Get("scrollHeight").Int()
	cht := pnl.Get("clientHeight").Int()
	doScroll := top > (ht - cht - 1)
	return doScroll, ht - cht
}
