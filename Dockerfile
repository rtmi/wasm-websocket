# build stage
FROM golang:alpine AS build-env
COPY . /go/src/patterns/wasm
RUN apk --no-cache add git; \
    cd /go/src/patterns/wasm; \
    go get github.com/gorilla/websocket; \
    go get github.com/btcsuite/btcd/btcec; \
    go get github.com/btcsuite/btcd/chaincfg; \
    go get github.com/btcsuite/btcutil; \
    go get github.com/bwmarrin/discordgo; \
    GOOS=js GOARCH=wasm go build -o www/test.wasm www/main.go; \
    go build -o www/wssrv *.go; \
    cp www/wasm_exec.html www/home.html;

# final stage 
FROM alpine
ENV SPEECH_TOKEN pass-from-shell
COPY --from=build-env /go/src/patterns/wasm/www/* /usr/local/wasm/
WORKDIR /usr/local/wasm
ENTRYPOINT ["/usr/local/wasm/wssrv"]

