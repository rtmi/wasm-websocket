// Trying to WebAssembly-ify the gorilla/websocket/examples/chat
//

package main

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"syscall/js"

	"patterns/wasm/chat"
)

var (
	beforeUnloadCh = make(chan struct{})
	scr            *chat.ChatScripter
)

const (
	ROOMLOG_ID  = "roomlog"
	ATTACHJS_ID = "attachToSend"
	WSURL       = "ws://localhost:8080/ws"
	VANITY_CMD  = "!vanity"
	VANITY_LMT  = 4
	WATSON_CMD  = "!watson"
)

func main() {
	scr = chat.NewChatScripter(ROOMLOG_ID)
	scr.Connect(WSURL)

	callback := js.NewCallback(dispatchRequest)
	defer callback.Release()
	attachToJsHandle := js.Global().Get(ATTACHJS_ID)
	attachToJsHandle.Invoke(callback)

	beforeUnloadCb := js.NewEventCallback(0, beforeUnload)
	defer beforeUnloadCb.Release()
	addEventListener := js.Global().Get("addEventListener")
	addEventListener.Invoke("beforeunload", beforeUnloadCb)

	<-beforeUnloadCh
	log.Println("Done")
}

// dispatchRequest accepts input from frontend and decide how to branch logic
func dispatchRequest(args []js.Value) {
	m, err := extractArg(args)
	if err != nil {
		log.Fatalf("Bad message input: %v\n", err)
		return
	}
	if strings.HasPrefix(m, "!") {
		runCommand(m)
	} else {
		scr.RecordMessage(m)
	}
}

// TODO check user has privileges for specified command
func runCommand(cmd string) {
	ar := strings.Split(cmd, " ")
	switch ar[0] {
	case VANITY_CMD:
		runVanityCmd(cmd)
	case WATSON_CMD:
		runWatsonCmd(cmd)
	default:
		log.Printf("Sorry command hasn't been implemented yet: %v\n", cmd)
	}
}

func runWatsonCmd(cmd string) {
	// todo more interesting task, but just say hello for now
	scr.HelloWatson(cmd)
}

func runVanityCmd(cmd string) {
	param := strings.TrimPrefix(cmd, VANITY_CMD)
	if cmd != param {
		p := strings.TrimSpace(param)

		if len(p) == 0 {
			log.Println("Vanity command was called without any prefix pattern")
			return
		}
		if len(p) > VANITY_LMT {
			log.Printf("Vanity prefix length is too much for performance reasons: %v max chars allowed\n", VANITY_LMT)
			return
		}
		if !strings.HasPrefix(p, "1") && !strings.HasPrefix(p, "3") {
			log.Println("Vanity prefix pattern can only begin with '1' and '3'")
			return
		}
		vanity, attempts, err := scr.CalcVanityAddress(p)
		if err != nil {
			log.Println(err)
			return
		}
		result := fmt.Sprintf("Address found: %v (after %v attempts)", vanity, attempts)
		scr.RecordMessage(result)

	}
}

func beforeUnload(event js.Value) {
	beforeUnloadCh <- struct{}{}
}

func extractArg(args []js.Value) (string, error) {
	if len(args) == 0 {
		return "", errors.New("Arg  required")
	}
	a := args[0].String()
	if strings.TrimSpace(a) == "" {
		return "", errors.New("Arg required")
	}
	return a, nil
}
