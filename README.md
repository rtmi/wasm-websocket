# wasm-websocket

WebAssembly of github.com/gorilla/websocket/examples/chat


## Quickstart

1. git clone https://github.com/patterns/wasm-websocket
2. docker build --network=host -t wasmsock wasm-websocket
3. Optionally, add the free Lite plan
 [Watson Text to Speech API](https://console.bluemix.net/docs/services/text-to-speech/getting-started.html)
4. Optionally, request token:

```
curl -X GET --user <username:password> --output watson.token \
"https://stream.watsonplatform.net/authorization/api/v1/token?url=https://stream.watsonplatform.net/text-to-speech/api"
```

5. docker run -ti --rm --network=host -e SPEECH_TOKEN=$(<watson.token) wasmsock
6. Visit localhost:8080
7. Launch a second tab/browser to localhost:8080
8. Test commands:

```
!vanity 1KID
!watson testing ipsum lorem
```



## Credits

WebSocket example by
 [Gorilla](https://github.com/gorilla/websocket/tree/master/examples/chat)
 ([LICENSE](https://github.com/gorilla/websocket/blob/master/LICENSE))

Go's WASM
 [Start Files](https://github.com/golang/go/tree/master/misc/wasm)
